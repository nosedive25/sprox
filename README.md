##Sprox##

**Installing**

 1. Clone the repository
 2. In the SproxNode directory, run `npm install`
 3. Install Phantom and Casper with `npm install -g phantomjs casperjs`
 4. Install Grunt with `npm install -g grunt grunt-cli`
 4. Install mitmproxy with `pip install mitmproxy`
 5. If running OS X, you'll need to update Pythons OpenSSL library to enable mitmproxy to handle TLS 1.2 connections. Run `pip install pyopenssl --upgrade` to update it.
 
Run `grunt` to start Sprox. Open `localhost:3000` in a browser and login with your NetID and password.

**An Explanation for mitmproxy**

Mitmproxy (Man in the middle proxy), is needed in order to properly communicate with CBOARD's GET application due to new, stricter security policies implemented by the company. During the summer of 2015, CBOARD disabled TLSv1.0 connections on *some* of their servers. Unfortunately, at this time PhantomJS, a core component of Sprox supports only TLS v1.0 connections.

We first discovered this issue when Sprox began to occasionally crash, stating it could not find the Shibboleth login form. After a quick check of the logs, we concluded that Phantom was unable to load the GET entry URL. The error Phantom returned was perplexing, it stated that its connection was being dropped by the server itself. Further perplexing was that the error could be temporarily eliviated by changing networks or routing traffic through a VPN before ultimately failing once again. Also interesting was that the DNS servers used had no affect on our connection dropping.

We began our quest to find the root of the issue by simulating a Phantom browser through Firefox. In Firefox, we downgraded its TLS capabilities by changing `security.ls.version.max` from `3` (TLSv1.2) to `1` (TLSv1.0) in`about:config`. After requesting the GET to Shibboleth redirect URL multiple times (and switching up the network connection), we were able to simulate Phantoms issues: Firefox reported that the connection had unexpectedly closed.

From our tests in Firefox, we concluded that *sometimes* CBOARD's servers refuse TLSv1.0 connections. This proved to be the root of the issue, PhantomJS currently only speaks in TLSv1.0. We are not certain the cause of the TLSv1.0 connection refusal, but all signs point to a poorly configured load balancing system. A quick dig indicated that `get.cbord.com` points to only one IP address (206.55.122.126), somewhat strange for a service that seems quite widely used. We assume that the IP address is one of a load balancer, and that *some* of the machines in CBOARD's load balancer pool are improperly configured. Based on our tests, we assume around around 30% of CBOARD's servers are configured to reject TLSv1.0 (For some reason...). The others however seem happy to accept it. If the IP address does in fact belong to a load balancer, it would explain A) The persistence of our dropped connections (We would consistently be assigned to the same misconfigured server over the course of one 'session') and B) The affect that changing our network connection or using a VPN had on our connection (We would be assigned to a new server in the pool).

So, how does this tie into mitmproxy? Well, asside from using it to help track down some of the finer points in regards to the connection dropping issue, it now acts as our 'SSL bridge.' 

When a user logs into Sprox, the user is assigned a temporary port number and an instance of mitmproxy is spawned on said port. The proxy is configured to 'intercept' SSL connections and re-sign them with a self signed certificate it generates on-the-fly. When a request to GET is made, Phantom makes the call through the proxy, leaving the proxy to deal with all external SSL interactions. The proxy fully supports TLSv1.2 and can talk to any of the servers in CBOARDS pool without issue. The primary connection (existing only between mitmproxy and Phantom) is fully compatible with TLSv1.0, allowing Phantom to read the contents of a users GET account.

This is a far less than optimal solution and is hacky at best. It will be removed as soon as possible. Currently, PhantomJS is undergoing development of version 2.1. Version 2.1 among other things, will include full support for TLSv1.2 connections. When Phantom 2.1 is released, mitmproxy will be removed from Sprox.